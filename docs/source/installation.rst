Installation
===================

Docker
-----------------

This project was built to run in a docker container. The Dockerfile present in the main folder makes use of the `Simulated Generic Motor Image <https://codebase.helmholtz.cloud/hzb/epics/ioc/images/simmotorgenericimage>`_ from HZB, so the required modules are based on this image.

Additionally, to communicate with PMAC devices, one must install the `PMAC Module for EPICS <https://github.com/dls-controls/pmac>`_. For that the commands inside the Dockerfile are used.

.. literalinclude:: ../../Dockerfile
    :language: bash

To run the ioc, do the following:

#. Install docker from `here <https://docs.docker.com/get-docker/>`_
#. Run on the terminal

    .. code-block:: bash

        docker pull registry.hzdr.de/messlabor/ioc-pmac-trigger

#. Start your container

    .. code-block:: bash

        docker run -it --name "temp" registry.hzdr.de/messlabor/ioc-pmac-trigger bash

#. Run the commands to download and build the ioc.

    .. code-block:: bash

        git clone https://codebase.helmholtz.cloud/messlabor/ioc-pmac-trigger.git /opt/epics/ioc/ioc-pmac-trigger
        make -C /opt/epics/ioc/ioc-pmac-trigger clean install

.. note:: 
    This project is used in the Helmholtz Coil Experiment of the Undulators group at BESSY II. There, due to constraints of the multimeter, which could not be containerized in Docker, a new installation was made in the host machine.s