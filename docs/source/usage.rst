Usage
=================

Settings
------------------

In the settings file "/iocBoot/iocIOC_MotorRecord/st.cmd" it is crucial to configure the IP settings to which EPICS should look for the device. Additionally, one must create the motor, the axis, the coordinate system and the coordinate system axis. These are settings pre-configured in the Power PMAC Project, that must be built and downloaded to the PMAC device. Here we have used the `PowerPmac2 Project <https://codebase.helmholtz.cloud/messlabor/powerpmac-trigger.git>`_.

.. literalinclude:: ../../iocBoot/iocIOC_MotorRecord/st.cmd

.. note::

    The functions are documented in the `PMAC EPICS Module <https://github.com/dls-controls/pmac.git>`_ from dls

User Interface
------------------

One can start the motor UI, after installing Java and Phoebus, by issuing, for example:

.. code-block:: bash
    
    ./phoebus.sh -resource file:/opt/epics/ioc/ioc-pmac-trigger/bob/motorx_all.bob?"P=PMAC:&M=M1"
    ./phoebus.sh -resource file:/opt/epics/ioc/ioc-pmac-trigger/usage/motorx_all.bob?"P=PMAC:&M=M2"

An Overview interface is available with information from the motor, ethercat and the trigger.

.. code-block:: bash

    ./phoebus.sh -resource file:/opt/epics/ioc/ioc-pmac-trigger/bob/overview.bob?

Below an image of the User Interface:

.. image:: figures/overview_screen.png
   :alt: User Interface
   :align: center

It is strcutured in four main parts:
* **Pmac CMD Motor**: with basic functionalitites for starting and killing the motor, as well as Jog forward and backward. The command box allow the user to send pmac commands, but note it does not give the response. This is useful mostly for resetting, for example "\$\$\$".
* **Ethercat**: with information about the Ethercat connection, such as the state of the connection and the buttons to enable or disable the plc which restores connection.
* **Trigger**: with information about the trigger, such as the state of the trigger, the important parameters to be defined and the buttons to enable or disable the trigger.
* **Motor**: with information about the motor, such as the state of the motor, the position, the velocity and the acceleration. The buttons allow the user to move the motor, set the position, set the velocity and set the acceleration.

Additionally, at the bottom left, some examples of useful pmac commands are given, as well as weblinks to the gitlab, documentation and the pmac project.

.. note::

    A full example project, the Helmholtz Coil, is available at `Helmholtz Coil <https://codebase.helmholtz.cloud/messlabor/hhs-trigger-ui.git>`_.