Trigger System
===============

Records
------------

.. auto-epics-records:: ../IOC_MotorRecordApp/Db/trigger.db

Subroutine C
------------

To read the array of positions saved during triggering, we use an `aSub <https://epics.anl.gov/base/R7-0/5-docs/aSubRecord.html>`_ record called subExample (shown above) which will connect via ssh to the pmac device and read the array.

.. literalinclude:: ../../IOC_MotorRecordApp/src/dbSubExample.c
    :language: c