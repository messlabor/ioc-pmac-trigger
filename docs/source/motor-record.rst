Motor Record 
=================


Refer to the `Motor Record <https://epics.anl.gov/bcda/synApps/motor/motorRecord.html>`_ documentation for more information.

.. auto-epics-records:: ../IOC_MotorRecordApp/Db/motor.db