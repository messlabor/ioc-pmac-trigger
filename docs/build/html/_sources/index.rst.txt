

Documentation!
===================================

This Epics IOC was created to communicate via etherent to the PMAC Omron CK3E-1310. It is used to control 1 motor using the motor record and to run a PLC program on the PMAC responsible for sending trigger signals 5-0V to a digital multimeter. Additionally, it is possible to monitor the EtherCAT connection and the PMAC status.

.. note::
   This project is strongly connected to the `Powerpmac Trigger <https://codebase.helmholtz.cloud/messlabor/powerpmac-trigger>`_ project.

Check out the :doc:`usage` section for further information.

.. note::

   This project is under active development.

Contents
--------

.. toctree:: 
   installation
   usage
   ethercat
   motor-record
   trigger

