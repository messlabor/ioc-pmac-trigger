#!../../bin/linux-x86_64/IOC_MotorRecord

#- You may have to change IOC_MotorRecord to something else
#- everywhere it appears in this file
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
epicsEnvSet("EPICS_CA_ADDR_LIST", "localhost")
epicsEnvSet("EPICS_CA_SERVER_PORT", "5064")

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/IOC_MotorRecord.dbd"

IOC_MotorRecord_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/motor.db","P=PMAC,M=M1")
dbLoadRecords("db/ecat.db", "P=PMAC,M=ECAT")
dbLoadRecords("db/trigger.db", "P=PMAC,M=TRIGGER")

## Configure motor control Power PMAC
# drvAsynPowerPMACPortConfigure(const char *portName,const char *hostName,const char *userName,const char *password,unsigned int priority,int noAutoConnect,int noProcessEos)
drvAsynPowerPMACPortConfigure("port1", "192.168.0.200", "root", "deltatau", "0", "0", "0")

# pmacCreateController(const char *portName, const char *lowLevelPortName, int lowLevelPortAddress,int numAxes, int movingPollPeriod, int idlePollPeriod)
pmacCreateController("m1", "port1", 0, 1, 100, 500)

# pmacCreateAxes(const char *pmacName, int numAxes)
pmacCreateAxis("m1", 1)
 
# Set the debug level (Controller Port, Debug level, Motor number (or 0 for controller), CS number (or 0 for real motors))
pmacDebug("m1", 2, 0, 0)

# pmacCreateCS(const char *portName,const char *controllerPortName,int csNo,int program)
pmacCreateCS("CS1", "m1", 1, 1)

# pmacCreateCSAxis 'Controller port name' 'Axis number'(1=A, 2=B, 3=C, 4=U, 5=V, 6=W, 7=X, 8=Y, 9=Z).       
pmacCreateCSAxis("CS1", 7)

# Set the PMAC CS axis scale factor (CS Port, Axis Number, Scale factor)
pmacSetCoordStepsPerUnit("CS1", 7, 10000)

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=gui"
