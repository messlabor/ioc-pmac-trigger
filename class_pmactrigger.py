
from ophyd import Component as Cpt
from ophyd import EpicsSignal, EpicsSignalRO, Device, EpicsMotor
from ophyd.status import SubscriptionStatus

from time import sleep


class PmacMotor(EpicsMotor):

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        # self.readback.name = self.name 

    cmd     = Cpt(cls = EpicsSignal, suffix = ":cmd",  kind = "omitted")
    jogf    = Cpt(cls = EpicsSignal, suffix = ".JOGF", kind = "omitted")
    jogr    = Cpt(cls = EpicsSignal, suffix = ".JOGR", kind = "omitted")
    # stop    = Cpt(cls = EpicsSignal, suffix = ".STOP", kind = "omitted")
    jvel    = Cpt(cls = EpicsSignal, suffix = ".JVEL", kind = "config")
    jar     = Cpt(cls = EpicsSignal, suffix = ".JAR",  kind = "config")
    hold    = Cpt(cls = EpicsSignal, suffix = ":ServoHold", write_pv = ":ServoHold.PROC")
    kill    = Cpt(cls = EpicsSignal, suffix = ":ServoKill", write_pv = ":ServoKill.PROC", kind = "omitted")


## -----  Trigger ------
class PmacTrigger(Device):

    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        # self.readback.name = self.name


    PlcActive   = Cpt(cls = EpicsSignalRO, suffix = "PLC_ACTIVE",           kind = "config")
    PlcRunning  = Cpt(cls = EpicsSignalRO, suffix = "PLC_RUNNING",          kind = "config")
    PlcEnable   = Cpt(cls = EpicsSignal,   suffix = "PLC_ENABLE.PROC",      kind = "omitted")
    PlcDisable  = Cpt(cls = EpicsSignal,   suffix = "PLC_DISABLE.PROC",     kind = "omitted")
    step_size   = Cpt(cls = EpicsSignal,   suffix = "step_size_Rd",         write_pv = "step_size_Wr",       kind = "config")
    cmd_run     = Cpt(cls = EpicsSignal,   suffix = "cmd_run_Rd",           write_pv = "cmd_run_Wr",         kind = "config")
    n_total     = Cpt(cls = EpicsSignal,   suffix = "n_total_Rd",           write_pv = "n_total_Wr",          kind = "config")
    Output      = Cpt(cls = EpicsSignalRO, suffix = "TriggerOutput_Rd",     kind = "hinted")
    Positions   = Cpt(cls = EpicsSignal,   suffix = "Positions.VALA",       write_pv = "Positions.PROC",    kind = "hinted")
    Times       = Cpt(cls = EpicsSignal,   suffix = "TimeBatch.VALA",       write_pv = "TimeBatch.PROC",    kind = "hinted")
    state       = Cpt(cls = EpicsSignalRO, suffix = "state_Rd",             kind = "hinted")
    multiturn   = Cpt(cls = EpicsSignal,   suffix = "multiturn_Rd",         write_pv = "multiturn_Wr",       kind = "config")
    cmd_abort   = Cpt(cls = EpicsSignal,   suffix = "cmd_abort_Rd",         write_pv = "cmd_abort_Wr",       kind = "config")
    home_pos    = Cpt(cls = EpicsSignalRO, suffix = "home_pos_Rd",          kind = "config")
    done        = Cpt(cls = EpicsSignal,   suffix = "measurement_done_Rd",  write_pv = "measurement_done_Wr", kind = "config")
    reset_all   = Cpt(cls = EpicsSignal,   suffix = "reset_all_Rd",         write_pv = "reset_all_Wr",        kind = "config")

    def get_positions(self):
        self.Positions.put(1)
        sleep(1)
        arr_pos = self.Positions.get()
        arr_pos = list(arr_pos[0:int(self.n_total.get())])
        return arr_pos
    
    def get_times(self):
        self.Times.put(1)
        sleep(1)
        arr_time = self.Times.get()
        arr_time = list(arr_time[0:int(self.n_total.get())])
        return arr_time

    def plc_reset(self):
        self.PlcDisable.put(1)
        self.PlcEnable.put(1)
        
## -----  ECAT ------
class PmacEcat(Device):
    
    def __init__(self, prefix, *args, **kwargs):
        super().__init__(prefix, **kwargs)
        # self.readback.name = self.name

    State       = Cpt(cls = EpicsSignalRO,  suffix = "STATE",       kind = "omitted")
    Enable      = Cpt(cls = EpicsSignalRO,  suffix = "ENABLE",      kind = "omitted")
    PlcActive   = Cpt(cls = EpicsSignalRO,  suffix = "PLC_ACTIVE",  kind = "omitted")
    PlcRunning  = Cpt(cls = EpicsSignalRO,  suffix = "PLC_RUNNING", kind = "omitted")
    PlcEnable   = Cpt(cls = EpicsSignal,    suffix = "PLC_ENABLE",  kind = "omitted")
    PlcDisable  = Cpt(cls = EpicsSignal,    suffix = "PLC_DISABLE", kind = "omitted")